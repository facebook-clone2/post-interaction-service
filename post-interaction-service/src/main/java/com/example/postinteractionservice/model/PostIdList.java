package com.example.postinteractionservice.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PostIdList {
    private List<String> postIds;
}
