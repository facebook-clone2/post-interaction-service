package com.example.postinteractionservice.repository;

import com.example.postinteractionservice.entity.PostReactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PostReactionRepository extends JpaRepository<PostReactionEntity, Integer> {

    @Query("SELECT e.reactedOn, COUNT(e) FROM PostReactionEntity e WHERE e.postId = :postId GROUP BY e.reactedOn")
    List<Object[]> countByReactedOnForPostId(@Param("postId") String postId);

    Optional<PostReactionEntity> findByUserIdAndPostIdAndReactedOn(Integer userId, String postId, Integer reactedOn);
    Optional<PostReactionEntity> findByUserIdAndPostId(Integer userId, String postId);

}
