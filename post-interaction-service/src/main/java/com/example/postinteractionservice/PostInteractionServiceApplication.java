package com.example.postinteractionservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PostInteractionServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(PostInteractionServiceApplication.class, args);
	}

}
