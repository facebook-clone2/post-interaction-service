package com.example.postinteractionservice.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "post_reactions")
public class PostReactionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id")
    private Integer id;

    @Column(name = "post_id")
    private String postId;

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "reacted_on")
    private Integer reactedOn;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getReactedOn() {
        return reactedOn;
    }

    public void setReactedOn(Integer reactedWith) {
        this.reactedOn = reactedWith;
    }
}
