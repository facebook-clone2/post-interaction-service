package com.example.postinteractionservice.controller;

import com.example.postinteractionservice.model.PostIdList;
import com.example.postinteractionservice.model.PostReactionResponse;
import com.example.postinteractionservice.service.IPostReactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/v1.0/post-interaction")
@CrossOrigin
public class PostController {
    @Autowired private IPostReactionService postReactionService;

    @PostMapping("/react")
    public ResponseEntity<Integer> addReactionOnPost(@RequestParam Integer userId, @RequestParam String postId, @RequestParam Integer reactedOn) {
        log.info("Got call to add reaction on post with postId: {}, userId: {}, reactedOn: {}",postId, userId, reactedOn );
        return postReactionService.addReactionOnPost(userId, postId, reactedOn);
    }

    @GetMapping("/getReactionOnPost")
    public ResponseEntity<PostReactionResponse> getAllReactionOnPost(@RequestParam String postId){
        log.info("Got call to get all reaction on a post: {}", postId);
        return postReactionService.getResponseOnPost(postId);
    }

    @PostMapping("/getReactionOnPosts")
    public ResponseEntity<List<PostReactionResponse>> getReactionsOnPostList(@RequestBody PostIdList postIds){
        log.info("Got call to get reactions on posts: {}", postIds.getPostIds());
        return postReactionService.getReactionOnPosts(postIds.getPostIds());
    }
}
