package com.example.postinteractionservice.service.Impl;
import com.example.postinteractionservice.entity.PostReactionEntity;
import com.example.postinteractionservice.model.PostReactionResponse;
import com.example.postinteractionservice.repository.PostReactionRepository;
import com.example.postinteractionservice.service.IPostReactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class PostReactionServiceImplService implements IPostReactionService {
    @Autowired private PostReactionRepository postReactionRepository;
    public ResponseEntity<Integer> addReactionOnPost(Integer userId, String postId, Integer reactedOn){
        Optional<PostReactionEntity> existingEntry = postReactionRepository.findByUserIdAndPostId(userId, postId);
        if (existingEntry.isPresent()) {
            log.info("User already have an reaction on this post: {}, going to update the reaction with: {}", existingEntry.get(), reactedOn);
            PostReactionEntity existingEntity = existingEntry.get();
            existingEntity.setReactedOn(reactedOn);
            postReactionRepository.save(existingEntity);
            return new ResponseEntity<>(existingEntity.getId(), HttpStatus.OK);
        } else {
            PostReactionEntity entity = new PostReactionEntity();
            entity.setPostId(postId);
            entity.setUserId(userId);
            entity.setReactedOn(reactedOn);
            PostReactionEntity savedEntity = postReactionRepository.save(entity);
            return new ResponseEntity<>(savedEntity.getId(), HttpStatus.OK);
        }
//        log.info("Got call to add reaction on post with , postId: {}, reacted by userId: {}, reacted on: {}", userId, postId, reactedOn);
//        ResponseEntity<Integer> response = new ResponseEntity<>(HttpStatus.OK);
//        PostReactionEntity entity = new PostReactionEntity();
//        entity.setPostId(postId);
//        entity.setUserId(userId);
//        entity.setReactedOn(reactedOn);
//        PostReactionEntity savedEntity = postReactionRepository.save(entity);
//        response = new ResponseEntity<>(savedEntity.getId(), HttpStatus.OK);
//        return response;
    }

    public ResponseEntity<PostReactionResponse> getResponseOnPost(String postId){
        ResponseEntity<PostReactionResponse> response = new ResponseEntity<>(HttpStatus.OK);
        PostReactionResponse postReactionResponse = new PostReactionResponse();
        postReactionResponse.setPostId(postId);
        List<Object[]> result = postReactionRepository.countByReactedOnForPostId(postId);
//        log.info("Result from database is: {}", result.stream());
//        log.info("Response from database to get all reaction {}", postReactionRepository.countByReactedOnForPostId(postId));
        result.forEach(objectArray -> {
            log.info("Object array: {}->{}", objectArray[0], objectArray[1]);
            if(Integer.valueOf(objectArray[0].toString()) == 1){
                postReactionResponse.setOne(Integer.valueOf(objectArray[1].toString()));
            } else if (Integer.valueOf(objectArray[0].toString()) == 2) {
                postReactionResponse.setTwo(Integer.valueOf(objectArray[1].toString()));
            } else if (Integer.valueOf(objectArray[0].toString()) == 3) {
                postReactionResponse.setThree(Integer.valueOf(objectArray[1].toString()));
            } else if (Integer.valueOf(objectArray[0].toString()) == 4) {
                postReactionResponse.setFour(Integer.valueOf(objectArray[1].toString()));
            }else {
                postReactionResponse.setFive(Integer.valueOf(objectArray[1].toString()));
            }
        });
        log.info("postReactionResponse is: {}", postReactionResponse);
        response = new ResponseEntity<>(postReactionResponse, HttpStatus.OK);
        return response;
    }
    public ResponseEntity<List<PostReactionResponse>> getReactionOnPosts(List<String> postIds){
        log.info("Got call to get all the reaction on the list of post with ids: {}", postIds);
        ResponseEntity<List<PostReactionResponse>> response = new ResponseEntity<>(HttpStatus.OK);
        List<PostReactionResponse> postReactionResponseList = new ArrayList<>();
        for (String postId : postIds){
            log.info("Current postId is: {}", postId);
            ResponseEntity<PostReactionResponse> res = getResponseOnPost(postId);
            log.info("Got reactions on post: {}", res.getBody());
            postReactionResponseList.add(res.getBody());
        }
        response = new ResponseEntity<>(postReactionResponseList, HttpStatus.OK);
        return response;
    }

}
