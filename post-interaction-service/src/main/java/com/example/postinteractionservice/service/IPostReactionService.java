package com.example.postinteractionservice.service;


import com.example.postinteractionservice.model.PostReactionResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IPostReactionService {
    ResponseEntity<Integer> addReactionOnPost(Integer userID, String postId, Integer reactedOn);

    ResponseEntity<PostReactionResponse> getResponseOnPost(String postId);

    ResponseEntity<List<PostReactionResponse>> getReactionOnPosts(List<String> postIds);


}
